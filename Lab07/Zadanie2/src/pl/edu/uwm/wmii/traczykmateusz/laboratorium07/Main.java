package pl.edu.uwm.wmii.traczykmateusz.laboratorium07;
import pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk.Adres;

public class Main {

    public static void main(String[] args)
    {
        Adres adres1 = new Adres("Uliczna", 5, 3, "Miastuń", "76-564");
        Adres adres2 = new Adres("Alejkowa", 6, "Miastawa", "42-244");

        adres1.pokaz();
        adres2.pokaz();

        System.out.println(adres1.przed("76-564"));
        System.out.println(adres2.przed("76-244"));
    }
}
