package pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk;

public class Adres
{
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;

    //konstruktor z danymi opcjonalnymi
    public Adres(String Ulica, int NumerDomu, int NumerMieszkania, String Miasto, String KodPocztowy)
    {
        ulica=Ulica;
        numer_domu=NumerDomu;
        numer_mieszkania=NumerMieszkania;
        miasto=Miasto;
        kod_pocztowy=KodPocztowy;
    }

    //konstruktor bez danych opcjonalnych
    public Adres(String Ulica, int NumerDomu, String Miasto, String KodPocztowy)
    {
        ulica=Ulica;
        numer_domu=NumerDomu;
        miasto=Miasto;
        kod_pocztowy=KodPocztowy;
    }

    public void pokaz()
    {
        System.out.println(miasto + " " + kod_pocztowy);
        System.out.print(ulica + " " + numer_domu);
        if(numer_mieszkania!=0) System.out.print("/" + numer_mieszkania);
        System.out.println();
    }

    public boolean przed(String przedKodPocztowy)
    {
        String temp1=kod_pocztowy.replaceAll("-",""); //usuwanie myślnika
        String temp2=przedKodPocztowy.replaceAll("-","");

        int KodLiczba1 = Integer.parseInt(temp1); //zamiana na Int w celu porównania
        int KodLiczba2 = Integer.parseInt(temp2);

        if(KodLiczba1<KodLiczba2) return true;
        else return false;
    }
}
