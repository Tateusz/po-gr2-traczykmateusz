package pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk;

public class Osoba
{
    private String nazwisko;
    private int rokUrodzenia;

    public Osoba(String Nazwisko, int RokUrodzenia)
    {
        nazwisko=Nazwisko;
        rokUrodzenia=RokUrodzenia;
    }

    public String toString()
    {
        return nazwisko + " " + rokUrodzenia;
    }

    public String getNazwisko() 
	{
        return nazwisko;
    }

    public int getRokUrodzenia() 
	{
        return rokUrodzenia;
    }
}
