package pl.edu.uwm.wmii.traczykmateusz.laboratorium07;
import pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk.Nauczyciel;
import pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk.Student;

public class Main {

    public static void main(String[] args)
    {
        Student student1 = new Student("Andrzejewski", 1998, "Architektura");
        Nauczyciel nauczyciel1 = new Nauczyciel("Terka", 1978, 3170.20);

        System.out.println(student1.toString());
        System.out.println(student1.getNazwisko());
        System.out.println(student1.getRokUrodzenia());
        System.out.println(student1.getKierunek());

        System.out.println();

        System.out.println(nauczyciel1.toString());
        System.out.println(nauczyciel1.getNazwisko());
        System.out.println(nauczyciel1.getRokUrodzenia());
        System.out.println(nauczyciel1.getPensja());
    }
}
