package pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk;

public class Student extends Osoba
{
    private String kierunek;

    public Student(String Nazwisko, int RokUrodzenia, String Kierunek)
    {
        super(Nazwisko, RokUrodzenia);
        kierunek=Kierunek;
    }

    public String toString()
    {
        return super.toString() + " " + kierunek;
    }

    public String getKierunek() 
	{
        return kierunek;
    }
}
