package pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk;
import java.awt.Rectangle;

public class BetterRectangle extends Rectangle
{
    public BetterRectangle(int posX, int posY, int width, int height)
    {
        super();
        setLocation(posX,posY);
        setSize(width,height);
    }

    public int getPerimeter()
    {
        return (height*2 + width*2);
    }

    public int getArea()
    {
        return (height*width);
    }
}
