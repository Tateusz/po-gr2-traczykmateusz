package pl.edu.uwm.wmii.traczykmateusz.laboratorium07;
import pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk.BetterRectangle;

public class Main {

    public static void main(String[] args)
    {
        BetterRectangle rect1 = new BetterRectangle(3,2,12,6);
        System.out.println("Pole: " + rect1.getArea());
        System.out.println("Obwod: " + rect1.getPerimeter());
    }
}
