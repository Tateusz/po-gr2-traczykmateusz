package pl.edu.uwm.wmii.traczykmateusz.laboratorium05;
import java.util.ArrayList;
import java.util.Collections;

public class Zadania
{
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {

        ArrayList<Integer> wynik = new ArrayList();
        for (int i = 0; i < a.size(); i++) {
            wynik.add(a.get(i));
        }
        for (int i = 0; i < b.size(); i++) {
            wynik.add(b.get(i));
        }
        return wynik;
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {

        ArrayList<Integer> wynik = new ArrayList();
        int dlugosc;
        if (a.size() > b.size()) dlugosc = a.size();
        else dlugosc = b.size();

        for (int i = 0; i < dlugosc; i++) {
            if (a.size() > i) {
                wynik.add(a.get(i));
            }
            if (b.size() > i) {
                wynik.add(b.get(i));
            }
        }
        return wynik;
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> wynik = new ArrayList();

        Collections.sort(a);
        Collections.sort(b);

        int dlugosc;
        if (a.size() > b.size()) dlugosc = a.size();
        else dlugosc = b.size();

        int rozmiarA = 0, rozmiarB = 0;
        do {

            if (rozmiarA != a.size()) {
                if (a.get(rozmiarA) <= b.get(rozmiarB)) {
                    wynik.add(a.get(rozmiarA));
                    rozmiarA++;
                }

                if (rozmiarB != b.size()) {
                    if (b.get(rozmiarB) <= a.get(rozmiarA)) {
                        wynik.add(b.get(rozmiarB));
                        rozmiarB++;
                    }
                }
            }
        }while ((rozmiarA != a.size() && rozmiarB != b.size())) ;

        if(a.size() != rozmiarA) wynik.add(a.get(rozmiarA));
        if(b.size() != rozmiarB) wynik.add(b.get(rozmiarB));

        return wynik;
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> wynik = new ArrayList();
        for(int i=a.size()-1; i>=0; i--)
        {
            wynik.add(a.get(i));
        }
        return wynik;
    }

    /*tu możliwe, że źle zrozumiałem polecenie, bo przez chwilę myślałem, że może chodzi o listy wskaźnikowe
     ale nie byłem ostatecznie pewien czy mogę ich tu użyć*/
    public static void reverse(ArrayList<Integer> a){
        ArrayList<Integer> wynik = new ArrayList();
        for(int i=a.size()-1; i>=0; i--)
        {
            wynik.add(a.get(i));
        }
        System.out.println(wynik);
    }
}
