package pl.edu.uwm.wmii.traczykmateusz.laboratorium05;
import java.util.ArrayList;
import java.util.Collections;

public class Main
{

    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList();
        ArrayList<Integer> b = new ArrayList();
        int[] listA = {1, 4, 9, 16};
        int[] listB = {9,7,4,9,11};

        for(int i=0; i<listA.length; i++){
            a.add(listA[i]);
        }
        for(int i=0; i<listB.length; i++){
            b.add(listB[i]);
        }

        System.out.println("Lista A: " + a);
        System.out.println("Lista B: " + b);

        System.out.println("Zadanie 1: " + Zadania.append(a,b));
        System.out.println("Zadanie 2: " + Zadania.merge(a,b));
        System.out.println("Zadanie 3: " + Zadania.mergeSorted(a,b));
        System.out.println("Zadanie 4: " + Zadania.reversed(a));
        System.out.println("Zadanie 5: ");
        System.out.println(a);
        Zadania.reverse(a);

    }
}
