package pl.edu.uwm.wmii.traczykmateusz.laboratoriumKolokwium;
import java.util.*;

public class Zadanie3 {

    public static void main(String[] args)
    {
        int n;
        int maks = 0;
        int countMaks = 0;
        do{
            Scanner inN = new Scanner(System.in);
            System.out.print("Podaj rozmiar tablicy (1-150): ");
            n = inN.nextInt();
        }while(n<1 || n>150);

        int tab[] = new int[n];
        generuj(tab);

        for(int i=0; i<tab.length; i++)
        {
            if(tab[i] > maks)
            {
                maks = tab[i];
            }
        }

        for(int i=0; i<tab.length; i++)
        {
            if(tab[i] == maks)
            {
                countMaks++;
            }
        }
        System.out.print("Maksymalny element: " + maks + " Ilosc jego wystapien w tablicy: " + countMaks);
    }

    public static void generuj(int tab[])
    {
        Random r = new Random();
        for(int i=0; i<tab.length; i++)
        {
            tab[i] = r.nextInt(2109) -1000;
        }
        for(int i=0; i<tab.length; i++)
        {
            System.out.print(tab[i] + ", ");
        }
        System.out.print("\n ");
    }
}