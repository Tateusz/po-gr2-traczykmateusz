package pl.edu.uwm.wmii.traczykmateusz.laboratoriumKolokwium;
import java.util.*;

public class Zadanie4 {

    public static void main(String[] args)
    {
        int suma = 0;
        double srednia = 0;
        Scanner ciag = new Scanner(System.in);
        String liczbySlownie = ciag.nextLine();

        String[] rozdzielenie = liczbySlownie.split("");
        int[] liczbowo = new int[rozdzielenie.length];
        for (int i = 0; i < liczbowo.length-1; i++)
        {
            liczbowo[i] = Integer.parseInt(rozdzielenie[i]);
        }
        for (int i = 0; i < liczbowo.length-1; i++)
        {
           suma = suma + liczbowo[i];
        }
        srednia = suma/(liczbowo.length-1);

        System.out.println("Srednia nie wliczajac 0 do tablicy wynosi: " + srednia);
    }
}
