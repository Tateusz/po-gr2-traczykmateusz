package pl.edu.uwm.wmii.traczykmateusz.laboratoriumKolokwium;
import java.util.*;

public class Zadanie1 {

    public static void main(String[] args) {
        double c, b;
        Scanner wczytajDane = new Scanner(System.in);

        System.out.print("Podaj c: ");
        c = wczytajDane.nextInt();

        System.out.print("Podaj b: ");
        b = wczytajDane.nextInt();

        if (c != 0) {
            double delta = b * b - 4 * c * 4;
            System.out.println("Delta: " + delta);

            if (delta < 0)
            {
                System.out.println("Brak rozwiązań");
            }
            else if (delta == 0)
            {
                double x;
                x = -b / (2 * c);
                System.out.println("Jedno rozwiązanie x = " + x);
            }
            else
                {
                double x1 = (-b + Math.sqrt(delta)) / (2 * c);
                double x2 = (-b - Math.sqrt(delta)) / (2 * c);
                System.out.println("Istnieją dwa rozwiązania x1 = " + x1 + " i " + x2);
            }
        }
        }
    }

