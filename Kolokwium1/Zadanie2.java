package pl.edu.uwm.wmii.traczykmateusz.laboratoriumKolokwium;
import java.util.*;

public class Zadanie2 {

    public static void main(String[] args)
    {
        Scanner skanerDane = new Scanner(System.in);
        int tab[] = new int[10];
        System.out.println("Podaj 10 liczb rzeczywistych");
        for(int i=0; i<10; i++)
        {
            tab[i] = skanerDane.nextInt();
        }
        parzyste(tab);
    }

    public static void parzyste(int tab[])
    {
        for(int i=0; i<10; i=i+2)
        {
            System.out.printf(tab[i] + " ");
        }
    }
}
