package pl.edu.uwm.wmii.traczykmateusz.laboratorium08.imiajd.traczyk;

public class Fortepian extends Instrument
{
    public String toString()
    {
        return "Fortepian ";
    }

    public String dzwiek()
    {
        return "plim plim plom ";
    }
}
