package pl.edu.uwm.wmii.traczykmateusz.laboratorium08;
import java.util.ArrayList;
import pl.edu.uwm.wmii.traczykmateusz.laboratorium08.imiajd.traczyk.*;

public class InstrumentyMain
{
    public static void main(String[] args)
    {
        ArrayList<Instrument> orkiestra = new ArrayList<Instrument>();
        orkiestra.add(new Skrzypce());
        orkiestra.add(new Skrzypce());
        orkiestra.add(new Fortepian());
        orkiestra.add(new Flet());
        orkiestra.add(new Skrzypce());

        System.out.println("Dzwieki orkiestry: ");
        for(Instrument i: orkiestra)
        {
            System.out.println(i.dzwiek());
        }

        System.out.println("Instrumenty orkiestry: ");
        for(int i=0; i<orkiestra.size(); i++)
        {
            System.out.println(orkiestra.get(i).toString());
        }
    }

}