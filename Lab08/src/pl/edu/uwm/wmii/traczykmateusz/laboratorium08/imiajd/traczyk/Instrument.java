package pl.edu.uwm.wmii.traczykmateusz.laboratorium08.imiajd.traczyk;

import java.time.LocalDate;
import java.util.Objects;

public abstract class Instrument
{
    private String producent;
    private java.time.LocalDate rokProdukcji;

    public String getProducent()
    {
        return producent;
    }

    public LocalDate getRokProdukcji()
    {
        return rokProdukcji;
    }

    public abstract String dzwiek();

    public String toString()
    {
        return "Instrument " + "producent - '" + producent + " , rokProdukcji - " + rokProdukcji;
    }

    public boolean equals(Object objekt)
    {
        if (this == objekt) return true;
        if (objekt == null || getClass() != objekt.getClass()) return false;
        Instrument that = (Instrument) objekt;
        return Objects.equals(producent, that.producent) && Objects.equals(rokProdukcji, that.rokProdukcji);
    }
}
