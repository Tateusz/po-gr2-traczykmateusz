package pl.edu.uwm.wmii.traczykmateusz.laboratorium08;
import pl.edu.uwm.wmii.traczykmateusz.laboratorium08.imiajd.traczyk.*;
import java.time.LocalDate;
import java.util.*;

public class Main
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalski", "Jan", LocalDate.of(1982,3,23),true, 20000, LocalDate.of(1996,12,3));
        ludzie[1] = new Student("Nowak", "Małgorzata", LocalDate.of(1999,7,10), false, "informatyka", 3.6);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");
        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + " " + p.getImiona() + ": " + p.getOpis());
            System.out.println(p.getPlec() + " " + p.getDataUrodzenia());
        }
    }
}
