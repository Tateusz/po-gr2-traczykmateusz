package pl.edu.uwm.wmii.traczykmateusz.laboratorium08.imiajd.traczyk;

public class Skrzypce extends Instrument
{
    public String toString()
    {
        return "Skrzypce ";
    }

    public String dzwiek()
    {
        return "pling pling ";
    }
}
