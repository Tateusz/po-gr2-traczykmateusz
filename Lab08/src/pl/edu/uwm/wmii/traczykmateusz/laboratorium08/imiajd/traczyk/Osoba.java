package pl.edu.uwm.wmii.traczykmateusz.laboratorium08.imiajd.traczyk;
import java.time.LocalDate;

public abstract class Osoba
{
    public Osoba(String nazwisko, String imiona, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String getImiona()
    {
        return imiona;
    }

    public LocalDate getDataUrodzenia()
    {
        return dataUrodzenia;
    }

    public String getPlec()
    {
        if (this.plec == true)
            return "Mezczyzna";
        else
            return "Kobieta";
    }

    private String nazwisko;
    private String imiona;
    private java.time.LocalDate dataUrodzenia;
    boolean plec;
}
