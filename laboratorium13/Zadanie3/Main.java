package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;
import java.util.*;


public class Main {

    public static void main(String[] args)
    {
        HashMap<Integer, Student> studenci = new HashMap<Integer, Student>();

        studenci.put(4, new Student("db+", "Kowalski", 4));
        studenci.put(12, new Student("dst", "Nowicki", 12));
        studenci.put(8, new Student("db", "Zelecki", 8));
        studenci.put(7, new Student("ndst", "Bielucki", 7));

        List<Map.Entry<Integer, Student>> lista = new ArrayList<Map.Entry<Integer, Student>>(studenci.entrySet());

        System.out.println("Lista studentow: ");
        Student.wypisz(lista, studenci);
        System.out.println();

        System.out.println("Po usunieciu Nowickiego: ");
        Student.usun(studenci, 12);
        Student.wypisz(lista, studenci);
        System.out.println();

        System.out.println("Po dodaniu Wojtczaka: ");
        Student.dodajEdytuj(studenci,"bdb+", "Wojtczak", 3);
        Student.wypisz(lista, studenci);
        System.out.println();

        System.out.println("Po zmianie oceny Bieluckiego: ");
        Student.dodajEdytuj(studenci,"dst", "Bielucki", 7);
        Student.wypisz(lista, studenci);
        System.out.println();
    }
}

class Student {
    public Student(String ocena, String nazwisko, int id) {
        this.ocena = ocena;
        this.nazwisko = nazwisko;
        this.id = id;
    }

    public String toString() {
        return "[Nazwisko: " + nazwisko + " ocena: " + ocena + "]";
    }

    public String ocena;
    public String nazwisko;
    public int id;

    public static void usun(Map<Integer, Student> mapa, int klucz)
    {
        mapa.remove(klucz);
    }

    public static void dodajEdytuj(Map<Integer, Student> mapa, String ocena, String nazwisko, int id) {
        mapa.put(id, new Student(ocena, nazwisko, id));
    }

    public static void wypisz(List<Map.Entry<Integer, Student>> studenciLista, HashMap<Integer, Student> studenciMapa)
    {
        studenciLista = new ArrayList<Map.Entry<Integer, Student>>(studenciMapa.entrySet());
        Collections.sort(studenciLista, new Comparator<Map.Entry<Integer, Student>>()
                {
                    public int compare(Map.Entry<Integer, Student> Student1, Map.Entry<Integer, Student> Student2)
                    {
                        return Student1.getValue().nazwisko.compareTo(Student2.getValue().nazwisko);
                    }
                }
        );
        for (Map.Entry<Integer, Student> element : studenciLista) {
            System.out.println(element);
        }
    }
}