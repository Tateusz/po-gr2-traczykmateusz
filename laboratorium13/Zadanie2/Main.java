package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args)
    {
        Map<String, Student> studenci = new HashMap<String, Student>();

        studenci.put("Kowalski", new Student("db+"));
        studenci.put("Nowicki", new Student("dst"));
        studenci.put("Zelecki", new Student("db"));
        studenci.put("Bielucki", new Student("ndst"));

        System.out.println("Lista studentow: ");
        Student.wypisz(studenci);
        System.out.println();

        System.out.println("Po usunieciu Nowickiego: ");
        Student.usun(studenci, "Nowicki");
        Student.wypisz(studenci);
        System.out.println();

        System.out.println("Po dodaniu Wojtczaka: ");
        Student.dodajEdytuj(studenci,"Wojtczak", "bdb+");
        Student.wypisz(studenci);
        System.out.println();

        System.out.println("Po zmianie oceny Bieluckiego: ");
        Student.dodajEdytuj(studenci,"Bielucki", "dst");
        Student.wypisz(studenci);
        System.out.println();
    }
}

class Student
{
    public Student(String ocena)
    {
        this.ocena = ocena;
    }

    public String toString()
    {
        return "[ocena: " + ocena + "]";
    }
    private String ocena;

    public static void usun(Map<String, Student> mapa, String klucz)
    {
        mapa.remove(klucz);
    }

    public static void dodajEdytuj(Map<String, Student> mapa, String kluczNazwisko, String ocena)
    {
        mapa.put(kluczNazwisko, new Student(ocena));
    }

    public static void wypisz(Map<String, Student> mapa)
    {
        TreeMap<String, Student> sorted = new TreeMap<>();
        sorted.putAll(mapa);
        for (Map.Entry<String, Student> entry : sorted.entrySet())
        {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}