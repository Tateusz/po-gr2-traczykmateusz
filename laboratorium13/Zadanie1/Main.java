package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;
import java.util.*;

public class Main {
    public static void main(String[] args)
    {
        PriorityQueue<Zadanie> zadania = new PriorityQueue<Zadanie>(5, new ZadaniaPriorytety());

        Zadanie zadTest = new Zadanie("Zadanie testowe o priorytecie 5", 5);
        zadania.add(zadTest);

        Scanner priorytetIn = new Scanner(System.in);
        Scanner opisIn = new Scanner(System.in);
        Scanner swtich = new Scanner(System.in);
        String opcja = "";
        String opis = "";
        int priorytet = 0;

        System.out.println("Wybierz opcje: ");
        opcja = swtich.nextLine();

        switch (opcja) {
            case "dodaj priorytet opis":
                System.out.println("Podaj priorytet zadania: ");
                priorytet = priorytetIn.nextInt();
                System.out.println("Podaj opis zadania: ");
                opis = opisIn.nextLine();
                Zadanie zad = new Zadanie(opis,priorytet);
                zadania.add(zad);
                break;
            case "nastepne":
                while (!zadania.isEmpty())
                {
                    System.out.println(zadania.poll().getOpis());
                }
                break;
            case "zakoncz":
                break;
            default:
                System.out.println("Prosze podac poprawne zadanie");
        }
    }
}

    class ZadaniaPriorytety implements Comparator<Zadanie> {
        public int compare(Zadanie job1, Zadanie job2) {
            if (job1.priorytet < job2.priorytet)
                return 1;
            else if (job1.priorytet > job2.priorytet)
                return -1;
            return 0;
        }
    }

    class Zadanie {
        public String opis;
        public int priorytet;


        public Zadanie(String opis, int priorytet) {

            this.opis = opis;
            this.priorytet = priorytet;
        }

        public String getOpis() {
            return opis;
        }
    }
