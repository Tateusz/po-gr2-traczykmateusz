package pl.edu.uwm.wmii.traczykmateusz.laboratorium11;
import java.time.LocalDate;
import java.util.*;

public class Main
{
    //wywołania
    public static void main(String[] args)
    {
        Integer[] liczby = {2,1,8,3,1};
        Integer[] liczby2 = {1,2,3,4,5};
        Integer[] liczby3 = {17,5,9,1,1,9};
        LocalDate[] daty = {LocalDate.parse("2012-12-13"), LocalDate.parse("1994-10-22"), LocalDate.parse("2022-01-02")};
        LocalDate[] daty2 = {LocalDate.parse("1994-10-22"), LocalDate.parse("2012-12-13"), LocalDate.parse("2022-01-02")};
        LocalDate[] daty3 = {LocalDate.parse("2012-12-13"), LocalDate.parse("1994-10-22"), LocalDate.parse("2022-01-02")};
        System.out.println("Zadanie 3");
        System.out.println(ArrayUtil.isSorted(liczby));
        System.out.println(ArrayUtil.isSorted(liczby2));
        System.out.println(ArrayUtil.isSorted(daty));
        System.out.println(ArrayUtil.isSorted(daty2));

        System.out.println("Zadanie 4");
        System.out.println(ArrayUtil.binSearch(liczby2, 2));

        System.out.println("Zadanie 5 selectionSort");
        ArrayUtil.selectionSort(liczby);
        for (int i=0; i<liczby.length; i++)
        {
            System.out.print(liczby[i] + ", ");
        }
        System.out.println();
        ArrayUtil.selectionSort(daty);
        for (int i=0; i<daty.length; i++)
        {
            System.out.print(daty[i] + ", ");
        }
        System.out.println();

        System.out.println("Zadanie 6 mergeSort");
        ArrayUtil.mergeSort(liczby3);
        for (int i=0; i<liczby3.length; i++)
        {
            System.out.print(liczby3[i] + ", ");
        }
        System.out.println();
        ArrayUtil.mergeSort(daty3);
        for (int i=0; i<daty3.length; i++)
        {
            System.out.print(daty3[i] + ", ");
        }
    }
}

class ArrayUtil {
    //zadanie 3
    public static <T extends Comparable> boolean isSorted(T[] testTab) {
        for (int i = 0; i < testTab.length - 1; ++i) {
            if (testTab[i].compareTo(testTab[i + 1]) > 0)
                return false;
        }
        return true;
    }

    //zadanie 4
    public static <T extends Comparable> int binSearch(T[] testTab, T objekt) {
        int first = 0;
        int last = testTab.length - 1;
        int wynik = -1;

        while (first <= last) {
            int mid = (first + last) / 2;
            if (objekt == testTab[mid]) {
                wynik = mid;
                break;
            } else if (objekt.compareTo(testTab[mid]) < 0) {
                last = mid - 1;
            } else if (objekt.compareTo(testTab[mid]) > 0) {
                first = mid + 1;
            } else return -1;
        }
        return wynik;
    }

    //zadanie 5
    public static <T extends Comparable> void selectionSort(T[] testTab) {
        int najwieksze;
        Comparable temp;
        for (int i = testTab.length - 1; i >= 0; i--) {
            najwieksze = 0;
            for (int j = 0; j <= i; j++) {
                if (testTab[najwieksze].compareTo(testTab[j]) < 0) {
                    najwieksze = j;
                }
            }
            if (najwieksze != i) {
                temp = testTab[i];
                testTab[i] = testTab[najwieksze];
                testTab[najwieksze] = (T) temp;
            }
        }
    }

    //zadanie 6
    public static <T extends Comparable> void mergeSort(T[] testTab)
    {
        int firstIndex = 0;
        int lastIndex = testTab.length - 1;

        T[] temp = Arrays.copyOf(testTab, testTab.length);

        for (int j = 1; j <=lastIndex - firstIndex; j = 2 * j)
        {
            for (int i = firstIndex; i < lastIndex ; i += 2 * j)
            {
                int start = i;
                int middle = i + j - 1;
                int end = Integer.min(i + 2 * j - 1, lastIndex);

                int x = start, y = start, z = middle + 1;
                while (y <= middle && z <= end) {
                if (testTab[y].compareTo(testTab[z]) < 0)
                {
                    temp[x++] = testTab[y++];
                }
                else
                    {
                        temp[x++] = testTab[z++];
                    }
            }
                while (y <= middle)
                {
                    temp[x++] = testTab[y++];
                }
                for (y = start; y <= end; y++)
                {
                    testTab[y] = temp[y];
                }
            }
        }
    }
}


