package pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk;

public class NazwanyPunkt extends Punkt
{
    private String name;
    public NazwanyPunkt(int x, int y, String name)
    {
        super(x, y);
        this.name = name;
    }
    @Override
    public void show()
    {
        System.out.println(name + ":<" + x() + ", " + y() + ">");
    }
}
