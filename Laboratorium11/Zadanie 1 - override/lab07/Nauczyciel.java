package pl.edu.uwm.wmii.traczykmateusz.laboratorium07.pl.imiajd.traczyk;

public class Nauczyciel extends Osoba
{
    private double pensja;

    public Nauczyciel(String Nazwisko, int RokUrodzenia, double Pensja)
    {
        super(Nazwisko, RokUrodzenia);
        pensja=Pensja;
    }

    @Override
    public String toString()
    {
        return super.toString() + " " + pensja;
    }

    public double getPensja() 
	{
        return pensja;
    }
}
