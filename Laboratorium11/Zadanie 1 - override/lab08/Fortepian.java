package pl.edu.uwm.wmii.traczykmateusz.laboratorium08.imiajd.traczyk;

public class Fortepian extends Instrument
{
    @Override
    public String toString()
    {
        return "Fortepian ";
    }

    @Override
    public String dzwiek()
    {
        return "plim plim plom ";
    }
}
