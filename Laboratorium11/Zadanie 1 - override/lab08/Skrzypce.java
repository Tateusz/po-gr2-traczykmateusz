package pl.edu.uwm.wmii.traczykmateusz.laboratorium08.imiajd.traczyk;

public class Skrzypce extends Instrument
{
    @Override
    public String toString()
    {
        return "Skrzypce ";
    }

    @Override
    public String dzwiek()
    {
        return "pling pling ";
    }
}
