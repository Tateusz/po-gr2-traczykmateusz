package pl.edu.uwm.wmii.traczykmateusz.laboratorium08.imiajd.traczyk;

public class Flet extends Instrument
{
    @Override
    public String toString()
    {
        return "Flet ";
    }

    @Override
    public String dzwiek() //dzwieki instrumentow wziete z listy onomatopei
    {
        return "tu tu fiu ";
    }
}
