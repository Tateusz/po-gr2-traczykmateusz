package pl.edu.uwm.wmii.traczykmateusz.laboratorium11;

public class Pair<T> {
//zadanie 1
    public Pair() {
        first = null;
        second = null;
    }

    public Pair (T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }
    public T getSecond() {
        return second;
    }

    public void setFirst (T newValue) {
        first = newValue;
    }
    public void setSecond (T newValue) {
        second = newValue;
    }

    public static <T> void swap(Pair<T> para) {
        T temp = para.first;
        para.first = para.second;
        para.second = temp;
    }

    public T first;
    public T second;
}
//zadanie 2
class PairUtil<T> {

    public static <T> void swap(Pair<T> para) {
        T temp = para.first;
        para.first = para.second;
        para.second = temp;
        System.out.println(para.first + " " + para.second);
    }

}
