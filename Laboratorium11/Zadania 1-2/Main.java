package pl.edu.uwm.wmii.traczykmateusz.laboratorium11;

public class Main {

    public static void main(String[] args)
    {
        String[] words = { "Ala", "ma", "psa", "i", "kota" };
        Pair<String> mm = ArrayAlg.minmax(words);
        Pair<String> mm2 = ArrayAlg.minmax(words);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());

        System.out.println("Przed swap: " );
        System.out.println(mm.getFirst());
        System.out.println(mm.getSecond());
//zadanie 1 wywołanie
        Pair.swap(mm);


        System.out.println("Po swap: " );
        System.out.println(mm.getFirst());
        System.out.println(mm.getSecond());
//zadanie 2 wywołanie
        System.out.println("Po swap z PairUtil: " );
        PairUtil.swap(mm2);

    }
}

class ArrayAlg {

    public static Pair<String> minmax(String[] a)
    {
        if (a == null || a.length == 0) {
            return null;
        }

        String min = a[0];
        String max = a[0];

        for (int i = 1; i < a.length; i++) {
            if (min.compareTo(a[i]) > 0) {
                min = a[i];
            }

            if (max.compareTo(a[i]) < 0) {
                max = a[i];
            }
        }

        return new Pair<String> (min, max);
    }
}

