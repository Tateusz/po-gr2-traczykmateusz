package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;
import java.util.LinkedList;

public class Zadanie3_4
{
    public static void odwroc(LinkedList<String> pracownicy)
    {
        LinkedList<String> odwrocona = new LinkedList<String>();
        for (int i=pracownicy.size()-1; i>=0; i--)
        {
            odwrocona.add(pracownicy.get(i));
        }
        System.out.println(odwrocona);
    }

    public static <T> void odwrocGeneric(LinkedList<T> pracownicy)
    {
        LinkedList<T> odwrocona = new LinkedList<>();
        for (int i=pracownicy.size()-1; i>=0; i--)
        {
            odwrocona.add(pracownicy.get(i));
        }
        System.out.println(odwrocona);
    }
}
