package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;
import java.util.LinkedList;

public class Zadanie1_2 {
    public static void redukuj(LinkedList<String> pracownicy, int n)
    {
        for(int i=n; i<pracownicy.size(); i=i+n)
        {
            pracownicy.remove(i);
        }
        System.out.println(pracownicy);
    }

    public static <T> void redukujGeneric(LinkedList<T> pracownicy, int n)
    {
        for(int i=n; i<pracownicy.size(); i=i+n)
        {
            pracownicy.remove(i);
        }
        System.out.println(pracownicy);
    }
}
