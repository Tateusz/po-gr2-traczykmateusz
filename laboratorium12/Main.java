package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;
import java.util.*;

public class Main {
    static void reverse(String zdanie) {
        Stack<String> stos = new Stack<>();
        String temp = "";
        for (int i = 0; i < zdanie.length(); i++) {
            if (zdanie.charAt(i) == ' ') {
                stos.add(temp);
                temp = "";
            } else {
                temp = temp + zdanie.charAt(i);
            }
        }
        stos.add(temp);

        while (!stos.isEmpty()) {
            temp = stos.peek();
            System.out.print(temp + " ");
            stos.pop();
        }
        System.out.print(". ");
    }

    Stack<String> stack = new Stack<String>();

    public static void main(String[] args) {
        //zadanie1
        System.out.println("Zadanie 1:");
        LinkedList<String> lista = new LinkedList<String>();
        lista.add("Kowalski");
        lista.add("Traczyk");
        lista.add("Dostojewski");
        lista.add("Hermaszewski");
        lista.add("Wybicki");
        lista.add("Potocki");
        lista.add("Zarzeski");
        lista.add("Nowakowski");
        lista.add("Toczek");
        System.out.println(lista);
        Zadanie1_2.redukuj(lista,2);

        //Zadanie2
        System.out.println("Zadanie 2:");
        Zadanie1_2.redukujGeneric(lista,2);//redukuje już wcześniej zredukowaną listę, żeby redukowało pełną wykomentować wywołanie zadania 1

        //Zadanie3
        System.out.println("Zadanie 3:");
        Zadanie3_4.odwroc(lista);

        //Zadanie4
        System.out.println("Zadanie 4:");
        //Zadanie3_4.odwrocGeneric(lista);

        //zadanie 5
        System.out.println("Zadanie 5: ");
        String zdanie = "Ala ma kota. Jej kot lubi myszy.";
        String[] podzial = zdanie.split("\\.");
        reverse(podzial[0]);
        reverse(podzial[1]);
        System.out.println();

        //zadanie 6
        System.out.println("Zadanie 6:");
        Zadanie6.cyfry(2015);
        System.out.println();

        //zadanie 7
        System.out.println("Zadanie 7:");
        Zadanie7.sito(20);
        System.out.println();
    }
}
