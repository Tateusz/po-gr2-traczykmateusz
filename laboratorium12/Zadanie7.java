package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;

import java.util.*;

public class Zadanie7
{
    static void sito(int n)
    {
        boolean pierwsze[] = new boolean[n+1];
        for(int i=0;i<n;i++)
        {
            pierwsze[i] = true;
        }
        for(int j=2; j*j<=n; j++)
        {
            if(pierwsze[j] == true)
            {
                for(int i=j*j; i<=n; i+=j)
                    pierwsze[i] = false;
            }
        }

        for(int i=2; i<=n; i++)
        {
            if(pierwsze[i] == true)
                System.out.print(i + " ");
        }
        System.out.print( " = Liczby pierwsze mniejsze od " + n);
    }
}
