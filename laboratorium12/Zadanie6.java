package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;
import java.util.*;

public class Zadanie6 {
    static void cyfry(int n)
    {
        Stack<Integer> stos = new Stack<>();
        while (n > 0)
        {
            int temp = 0;
            temp = n%10;
            stos.add(temp);
            n = n/10;
        }
        while (!stos.isEmpty()) {
            int elem = stos.peek();
            System.out.print(elem + " ");
            stos.pop();
        }
    }

}
