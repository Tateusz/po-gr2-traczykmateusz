package pl.edu.uwm.wmii.traczykmateusz.laboratorium00;

public class Zadania {

    public static void Zad01() {
        System.out.println(31 + 29 + 31);
    }

    public static void Zad02() {
        int wynik = 0;
        for (int i = 1; i <= 10; i = i + 1) {
            wynik = wynik + i;
        }
        System.out.println(wynik);
    }

    public static void Zad03() {
        int wynik = 1;
        for (int i = 1; i <= 10; i = i + 1) {
            wynik = wynik * i;
        }
        System.out.println(wynik);
    }

    public static void Zad04() {
        double procent = 0.06;
        double saldo = 1000;
        for (int i = 1; i <= 3; i = i = i + 1) {
            saldo = saldo + (saldo * procent);
            System.out.println(i + " rok: " + saldo);
        }
    }

    static public void Zad05() {
        System.out.println("+----+");
        System.out.println("|Java|");
        System.out.println("+----+");
    }

    static public void Zad06() {
        System.out.println("""
                  /////
                 +""\"""+
                (|o   o|)
                  | ^ |
                 | `-' | 
                 +-----+         
                """);
    }

    static public void Zad07() {
        System.out.println("""
                *         *    ******  *********  ******   *     *   *******   *******
                * *     * *    *    *      *      *        *     *   *             *
                *  *   *  *    ******      *      ******   *     *   *******     *
                *   * *   *    *    *      *      *        *     *         *   *
                *    *    *    *    *      *      ******   *******   *******   *******
                """);
    }

    static public void Zad08() {
        System.out.println("""
                   +
                  + +
                 +   +
                +-----+
                | .-. |
                | | | |
                +-+-+-+
                """);
    }

    static public void Zad09() {
        System.out.println("""
                 ^ ^       _________
                (o_o)/   / Hello  '
                |   |   |  Junior |
               (") (")   ' Coder!  /
                          ---------

                """);
    }

    public static void Zad10() {
        System.out.println("Star Wars: Revenge of the Sith\nSpiderman: Into the Spiderverse\nLove, Death and Robots");
    }

    public static void Zad11() {
        System.out.println("""
                Bolesław Leśmian
                                
                Dziewczyna
                                           
                Dwunastu braci, wierząc w sny, zbadało mur od marzeń strony,
                A poza murem płakał głos, dziewczęcy głos zaprzepaszczony.
                                
                I pokochali głosu dźwięk i chętny domysł o Dziewczynie,
                I zgadywali kształty ust po tym, jak śpiew od żalu ginie…
                                
                Mówili o niej: „Łka, więc jest!” – I nic innego nie mówili,
                I przeżegnali cały świat – i świat zadumał się w tej chwili…
                                
                                
                Porwali młoty w twardą dłoń i jęli w mury tłuc z łoskotem!
                I nie wiedziała ślepa noc, kto jest człowiekiem, a kto młotem?
                                
                „O, prędzej skruszmy zimny głaz, nim śmierć Dziewczynę rdzą powlecze!” –
                Tak, waląc w mur, dwunasty brat do jedenastu innych rzecze.
                                
                Ale daremny był ich trud, daremny ramion sprzęg i usił!
                Oddali ciała swe na strwon owemu snowi, co ich kusił!
                                
                Łamią się piersi, trzeszczy kość, próchnieją dłonie, twarze bledną…
                I wszyscy w jednym zmarli dniu i noc wieczystą mieli jedną!
                                
                Lecz cienie zmarłych – Boże mój! – nie wypuściły młotów z dłoni!
                I tylko inny płynie czas – i tylko młot inaczej dzwoni…
                                
                I dzwoni w przód! I dzwoni wspak! I wzwyż za każdym grzmi nawrotem!
                I nie wiedziała ślepa noc, kto tu jest cieniem, a kto młotem?
                                
                „O, prędzej skruszmy zimny głaz, nim śmierć Dziewczynę rdzą powlecze!” –
                Tak, waląc w mur, dwunasty cień do jedenastu innych rzecze.
                                
                Lecz cieniom zbrakło nagle sił, a cień się mrokom nie opiera!
                I powymarły jeszcze raz, bo nigdy dość się nie umiera…
                                
                I nigdy dość, i nigdy tak, jak pragnie tego ów, co kona!…
                I znikła treść – i zginął ślad – i powieść o nich już skończona!
                                
                Lecz dzielne młoty – Boże mój – mdłej nie poddały się żałobie!
                I same przez się biły w mur, huczały śpiżem same w sobie!
                                
                Huczały w mrok, huczały w blask i ociekały ludzkim potem!
                I nie wiedziała ślepa noc, czym bywa młot, gdy nie jest młotem?
                                
                „O, prędzej skruszmy zimny głaz, nim śmierć Dziewczynę rdzą powlecze!” –
                Tak, waląc w mur, dwunasty młot do jedenastu innych rzecze.
                                
                I runął mur, tysiącem ech wstrząsając wzgórza i doliny!
                Lecz poza murem – nic i nic! Ni żywej duszy, ni Dziewczyny!
                                
                Niczyich oczu ani ust! I niczyjego w kwiatach losu!
                Bo to był głos i tylko – głos, i nic nie było oprócz głosu!
                                
                Nic – tylko płacz i żal i mrok i niewiadomość i zatrata!
                Takiż to świat! Niedobry świat! Czemuż innego nie ma świata?
                                
                Wobec kłamliwych jawnie snów, wobec zmarniałych w nicość cudów,
                Potężne młoty legły w rząd, na znak spełnionych godnie trudów.
                                
                I była zgroza nagłych cisz. I była próżnia w całym niebie!
                A ty z tej próżni czemu drwisz, kiedy ta próżnia nie drwi z ciebie?
                """);
    }

    public static void Zad12() {
        System.out.println("""
                *   *   *   *   *   *__________________________________
                  *   *   *   *   *  __________________________________
                *   *   *   *   *   *__________________________________
                  *   *   *   *   *  __________________________________
                *   *   *   *   *   *__________________________________
                  *   *   *   *   *  __________________________________
                *   *   *   *   *   *__________________________________
                  *   *   *   *   *  __________________________________
                *   *   *   *   *   *__________________________________
                _______________________________________________________
                _______________________________________________________
                _______________________________________________________
                _______________________________________________________
                _______________________________________________________
                _______________________________________________________
                _______________________________________________________
                """);
    }
}