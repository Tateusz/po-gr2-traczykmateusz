package pl.edu.uwm.wmii.traczykmateusz.laboratorium10;
import pl.edu.uwm.wmii.traczykmateusz.laboratorium10.imiajd.traczyk.*;
import java.time.LocalDate;
import java.util.*;

public class TestStudent
{
    public static void main(String[] args)
    {
        LocalDate st1 = LocalDate.of(1992, 10, 11);
        LocalDate st2 = LocalDate.of(1986, 3, 2);
        LocalDate st3 = LocalDate.of(2002, 7, 3);
        LocalDate st4 = LocalDate.of(1999, 1, 10);
        LocalDate st5 = LocalDate.of(1992, 10, 11);

        ArrayList<Student> grupaSt = new ArrayList<Student>();

        grupaSt.add(new Student("Kowalski", st1, 3.40));
        grupaSt.add(new Student("Nowak", st2, 4.20));
        grupaSt.add(new Student("Kowalski", st3, 3.0));
        grupaSt.add(new Student("Traczyk", st4, 4.20));
        grupaSt.add(new Student("Bralewski", st5, 4.80));

        System.out.println("Przed sortowaniemu: ");
        for (int i = 0; i < 5; i++) {
            System.out.println(grupaSt.get(i).toString());
        }

        Collections.sort(grupaSt);

        System.out.println("Po sortowaniu: ");
        for (int i = 0; i < 5; i++) {
            System.out.println(grupaSt.get(i).toString());
        }
    }
}
