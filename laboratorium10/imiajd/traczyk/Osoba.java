package pl.edu.uwm.wmii.traczykmateusz.laboratorium10.imiajd.traczyk;
import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba>
{
    public Osoba(String nazwisko, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String toString()
    {
        return "Osoba " + this.nazwisko + ", " + this.dataUrodzenia;
    }

    public boolean equals(Osoba obj)
    {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != this.getClass())
        {
            return false;
        }
        final Osoba other = (Osoba) obj;
        if ((this.nazwisko == null) ? (other.nazwisko != null) : !this.nazwisko.equals(other.nazwisko))
        {
            return false;
        }
        if (this.dataUrodzenia != other.dataUrodzenia)
        {
            return false;
        }
        return true;
    }

    public int compareTo(Osoba other)
    {
       int result = this.nazwisko.compareTo(other.nazwisko);
       if(result != 0)
       {
           return result;
       }
       if(result == 0)
       {
           result = this.dataUrodzenia.compareTo(other.dataUrodzenia);
       }
       if(result != 0)
       {
           return result;
       }
       return result;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;

    //żeby móc użyć prywatnego pola w toString w klasie Student, nie wiedziałem czy mam wypisać to w taki akurat sposób ale brzmiało najbardziej rozsądnie
    public Osoba(String nazwisko) { this.nazwisko = nazwisko; }
    public String getNAZW() {return nazwisko;}

    public Osoba(LocalDate dataUrodzenia) { this.dataUrodzenia = dataUrodzenia; }
    public LocalDate getDU() {return dataUrodzenia;}
}
