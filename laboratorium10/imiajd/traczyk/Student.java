package pl.edu.uwm.wmii.traczykmateusz.laboratorium10.imiajd.traczyk;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>
{

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen)
    {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public String toString()
    {
        return "Osoba " + getNAZW() + ", " + getDU() + ", " + this.sredniaOcen;
    }

    private double sredniaOcen;
}
