package pl.edu.uwm.wmii.traczykmateusz.laboratorium10;
import pl.edu.uwm.wmii.traczykmateusz.laboratorium10.imiajd.traczyk.*;
import java.time.LocalDate;
import java.util.*;

public class TestOsoba
{

    public static void main(String[] args)
    {
        LocalDate os1 = LocalDate.of(1992,10,11);
        LocalDate os2 = LocalDate.of(1986,3,2);
        LocalDate os3 = LocalDate.of(2002,7,3);
        LocalDate os4 = LocalDate.of(1999,1,10);
        LocalDate os5 = LocalDate.of(1992,10,11);

        ArrayList<Osoba> grupa = new ArrayList<Osoba>();

        grupa.add(new Osoba("Walczak", os1));
        grupa.add(new Osoba("Nowak",os2));
        grupa.add(new Osoba("Walczak",os3));
        grupa.add(new Osoba("Traczyk",os4));
        grupa.add(new Osoba("Zarzeski",os5));

        System.out.println("Przed sortowaniemu: ");
        for(int i=0; i<5; i++)
        {
            System.out.println(grupa.get(i).toString());
        }

        Collections.sort(grupa);
        System.out.println("Po sortowaniu: ");
        for(int i=0; i<5; i++)
        {
            System.out.println(grupa.get(i).toString());
        }
    }
}
