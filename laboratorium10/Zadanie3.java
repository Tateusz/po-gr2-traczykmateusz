package pl.edu.uwm.wmii.traczykmateusz.laboratorium10;
import java.io.*;
import java.util.*;

public class Zadanie3
{
    public static void main(String[] args) throws IOException
    {
        String wejsciePlik = "dane.txt";

        FileReader czytaj = new FileReader(wejsciePlik);
        BufferedReader brCzytaj = new BufferedReader(czytaj);

        String wejscieLinia;
        List<String> linie = new ArrayList<String>();
        while ((wejscieLinia = brCzytaj.readLine()) != null)
        {
            linie.add(wejscieLinia);
        }

        System.out.println("Plik przed posortowaniem: ");
        for(int i=0; i<linie.size(); i++)
        {
            System.out.print(linie.get(i) + ", ");
        }
        System.out.println();
        
        czytaj.close();
        Collections.sort(linie);

        System.out.println("Plik po posortowaniu: ");
        for(int i=0; i<linie.size(); i++)
        {
            System.out.print(linie.get(i) + ", ");
        }
    }
}
