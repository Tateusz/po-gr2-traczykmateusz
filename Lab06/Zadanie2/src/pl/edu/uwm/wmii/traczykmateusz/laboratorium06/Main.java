package pl.edu.uwm.wmii.traczykmateusz.laboratorium06;

public class Main {

    public static void main(String[] args)
    {
        int tab1[] = new int[]{1,3,5,6};
        int tab2[] = new int[]{2,3,6,8};

        IntegerSet test1 = new IntegerSet(tab1);
        IntegerSet test2 = new IntegerSet(tab2);

        IntegerSet Union = new IntegerSet(IntegerSet.union(test1.liczby, test2.liczby));
        System.out.println("Suma mnogosciowa: " + Union.toString());

        IntegerSet Inserction = new IntegerSet(IntegerSet.inserction(test1.liczby, test2.liczby));
        System.out.println("Iloczyn mnogosciowy: " + Inserction.toString());

        System.out.println("Tablica1 przed zmianami: " + test1.toString());

        test1.insertElement(25);
        System.out.println("Tablica1 po dodaniu elementu: " + test1.toString());

        test1.deleteElement(3);
        System.out.println("Tablica1 po usunieciu elementu: " + test1.toString());

        System.out.println(test1.equals(test1.liczby));
        System.out.println(test2.equals(test1.liczby));


    }
}
