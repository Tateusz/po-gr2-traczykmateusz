package pl.edu.uwm.wmii.traczykmateusz.laboratorium06;

public class IntegerSet
{
    Boolean liczby[] = new Boolean[100];

    public IntegerSet(int tab[])
    {
        for(int i=0; i<tab.length; i++)
        {
            liczby[tab[i]-1]=true;
        }

        for(int i=0; i<100; i++)
        {
            if(liczby[i] == null) liczby[i] = false;
        }
    }

    public IntegerSet(Boolean tab[])
    {
        for(int i=0; i<100; i++)
        {
            if(tab[i]==true) liczby[i] = true;
            else liczby[i] = false;
        }
    }

    public static Boolean[] union(Boolean tab1[], Boolean tab2[])
    {
        Boolean sumaMn[] = new Boolean[100];
        for(int i=0; i<100; i++)
        {
            if(tab1[i] == true || tab2[i] == true) sumaMn[i] = true;
            else sumaMn[i] = false;
        }
        return sumaMn;
    }

    public static Boolean[] inserction(Boolean tab1[], Boolean tab2[]){
        Boolean iloczynMn[] = new Boolean[100];
        for(int i=0; i<100; i++)
        {
            if(tab1[i] == true && tab2[i] == true) iloczynMn[i] = true;
            else iloczynMn[i]=false;
        }
        return iloczynMn;
    }


    public void insertElement(int liczba)
    {
        liczby[liczba-1]=true;
    }

    public void deleteElement(int liczba)
    {
        liczby[liczba-1]=false;
    }

    public String toString()
    {
        String show="";
        for(int i=0; i<100; i++)
        {
            if(liczby[i] == true)
                show+=(i+1 + " ");
        }
        return show;
    }

    public Boolean equals(Boolean tab[])
    {
        Boolean identyko=true;
        for(int i=0; i<100; i++)
        {
            if(tab[i] != liczby[i]) identyko=false;
            if(identyko==false) return false;
        }
        return identyko;
    }


}
