package pl.edu.uwm.wmii.traczykmateusz.laboratorium06;

public class RachunekBankowy
{
    static double rocznaStopaProcentowa;
    private double saldo;

    public void obliczMiesieczneOdsetki()
    {
        saldo+=(saldo*rocznaStopaProcentowa)/12;
    }

    public static void setRocznStopaProcentowa(int procent)
    {
        rocznaStopaProcentowa=procent;
    }

    public RachunekBankowy(double startSaldo)
    {
        saldo = startSaldo;
    }

    public double wypisz()
    {
        return saldo;
    }
}
