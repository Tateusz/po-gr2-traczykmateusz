package pl.edu.uwm.wmii.traczykmateusz.laboratorium06;
import java.text.DecimalFormat;

public class Main
{
    private static DecimalFormat f = new DecimalFormat("#.##");

    public static void main(String[] args)
    {
        RachunekBankowy saver1 = new RachunekBankowy(1200);
        RachunekBankowy saver2 = new RachunekBankowy(2400);

        RachunekBankowy.setRocznStopaProcentowa(4);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Pierwszy miesiac, stopa procentowa = 4%");
        System.out.println("Stan konta saver1: " + f.format(saver1.wypisz()));
        System.out.println("Stan konta saver2: " + f.format(saver2.wypisz()));

        RachunekBankowy.setRocznStopaProcentowa(5);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Drugi miesiac, stopa procentowa = 5%");
        System.out.println("Stan konta saver1: " + f.format(saver1.wypisz()));
        System.out.println("Stan konta saver2: " + f.format(saver2.wypisz()));


    }
}
