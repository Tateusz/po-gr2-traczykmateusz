package pl.edu.uwm.wmii.traczykmateusz.laboratorium02;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        int n,m,k;

        do{
            Scanner inN = new Scanner(System.in);
            System.out.print("N: ");
            n = inN.nextInt();
        }while(n<1 || n>10);
        do{
            Scanner inM = new Scanner(System.in);
            System.out.print("M: ");
            m = inM.nextInt();
        }while(m<1 || m>10);
        do{
            Scanner inK = new Scanner(System.in);
            System.out.print("K: ");
            k = inK.nextInt();
        }while(k<1 || k>10);

        Random losoweInt = new Random();

        int tabA[][] = new int[m][n];
        int tabB[][] = new int[n][k];
        int tabC[][] = new int[m][k];

        for(int i=0; i<m; i++)
        {
            for(int j=0; j<n; j++)
            {
                tabA[i][j] = losoweInt.nextInt(10);
            }
        }
        for(int i=0; i<n; i++)
        {
            for(int j=0; j<k; j++)
            {
                tabB[i][j] = losoweInt.nextInt(10);
            }
        }

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < k; j++)
            {
                int suma=0;
                for (int l = 0; l < n; l++)
                {
                    suma+=tabA[i][l]*tabB[l][j];
                }
                tabC[i][j] = suma;
            }
        }
        System.out.println("Macierz A:");
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < k; j++) {
                System.out.print(tabA[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Macierz B:");
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < k; j++) {
                System.out.print(tabB[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Macierz C - iloczyn macierzy A i B:");
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < k; j++) {
                System.out.print(tabC[i][j] + " ");
            }
            System.out.println();
        }
    }
}
