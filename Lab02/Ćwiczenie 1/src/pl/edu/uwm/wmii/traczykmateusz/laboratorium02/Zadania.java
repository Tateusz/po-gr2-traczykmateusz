package pl.edu.uwm.wmii.traczykmateusz.laboratorium02;
import java.util.*;

public class Zadania
{
    public static void generuj(int tab[])
    {
        Random r = new Random();
        for(int i=0; i<tab.length; i++)
        {
            tab[i] = r.nextInt(1999) -1000;
        }
        for(int i=0; i<tab.length; i++)
        {
            System.out.print(tab[i] + ", ");
        }
        System.out.print("\n ");
    }

    public void zadA(int tab[])
    {
        int nieparzyste=0, parzyste=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]==0) continue;
            else if(tab[i]%2==0) parzyste++;
            else nieparzyste++;
        }
        System.out.println("A)");
        System.out.println("Ilosc parzystych: " + parzyste);
        System.out.println("Ilosc nieparzystych: " + nieparzyste);
    }

    public void zadB(int tab[])
    {
        int dodatnie=0, ujemne=0, zerowe=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]>0) dodatnie++;
            else if(tab[i]<0) ujemne++;
            else zerowe++;
        }
        System.out.println("B)");
        System.out.println("Ilosc dodatnich: " + dodatnie);
        System.out.println("Ilosc ujemnych: " + ujemne);
        System.out.println("Ilosc zerowych: " + zerowe);
    }

    public void zadC(int tab[])
    {
        int maks=tab[0], ileRazy=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]>maks) maks = tab[i];
        }
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]==maks) ileRazy++;
        }
        System.out.println("C)");
        System.out.println("Najwiekszy element: " + maks);
        System.out.println("Ilosc jego wystapien: " + ileRazy);
    }

    public void zadD(int tab[])
    {
        int sumaUjemne=0, sumaDodatnie=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]>0) sumaDodatnie += tab[i];
            else sumaUjemne += tab[i];
        }
        System.out.println("D)");
        System.out.println("Suma elementow dodatnich: " + sumaDodatnie);
        System.out.println("Suma elementow ujemnych: " + sumaUjemne);
    }

    public void zadE(int tab[])
    {
        int najdluzszy=0, temp=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]>0) najdluzszy++;
            else if(tab[i]<=0 && najdluzszy>temp)
            {
                temp=najdluzszy;
                najdluzszy=0;
            }
            else najdluzszy=0;
        }
        System.out.println("E)");
        if(najdluzszy>temp) System.out.println("Dlugosc najdluzszego dodatniego fragmentu: " + najdluzszy);
        else System.out.println("Dlugosc najdluzszego dodatniego fragmentu: " + temp);
    }
    public void zadF(int tab[])
    {
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]>0) tab[i] = 1;
            else if(tab[i]<0) tab[i] = -1;
        }
        System.out.println("F)");
        System.out.println("Tablica: ");
        for(int i=0; i<tab.length; i++)
        {
            System.out.println(tab[i] + " ");
        }
    }

    public void zadG(int tab[], int lewy, int prawy)
    {
        for(int i=lewy; i<prawy; i++)
        {
            int temp = tab[i];
            tab[i] = tab[prawy];
            tab[prawy] = temp;
            prawy--;
        }
        System.out.println("G)");
        for(int i=0; i<tab.length; i++)
        {
            System.out.print(tab[i] + ", ");
        }
        System.out.print("\n ");
    }
}
