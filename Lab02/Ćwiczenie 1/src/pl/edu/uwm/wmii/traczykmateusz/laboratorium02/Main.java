package pl.edu.uwm.wmii.traczykmateusz.laboratorium02;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        int n;
        do{
            Scanner inN = new Scanner(System.in);
            System.out.print("Podaj rozmiar tablicy (1-100): ");
            n = inN.nextInt();
        }while(n<1 || n>100);


        int tab[] = new int[n];
        Zadania zad = new Zadania();

        zad.generuj(tab);
        zad.zadA(tab);
        zad.zadB(tab);
        zad.zadC(tab);
        zad.zadD(tab);
        zad.zadE(tab);
        zad.zadG(tab,1,2);
        zad.zadF(tab);

    }
}
