package pl.edu.uwm.wmii.traczykmateusz.laboratorium02;
import java.util.*;

public class Main
{

    public static void main(String[] args)
    {
        int n;
        do{
            Scanner inN = new Scanner(System.in);
            System.out.print("Podaj rozmiar tablicy (1-100): ");
            n = inN.nextInt();
        }while(n<1 || n>100);
        Zadania zad = new Zadania();


        int tab[] = new int[n];
        zad.generuj(tab);
        System.out.println("Ilosc nieparzystych: " + zad.ileNieparzystych(tab));
        System.out.println("Ilosc parzystych: " + zad.ileParzystych(tab));
        System.out.println("Ilosc dodatnich: " + zad.ileDodatnich(tab));
        System.out.println("Ilosc ujemnych: " + zad.ileUjemnych(tab));
        System.out.println("Ilosc zerowych: " + zad.ileZerowych(tab));
        System.out.println("Wystapienia liczby maksymalnej: " + zad.ileMaksymalnych(tab));
        System.out.println("Suma dodatnich: " + zad.sumaDodatnich(tab));
        System.out.println("Suma ujemnych: " + zad.sumaUjemnych(tab));
        System.out.println("Dlugosc najdluzszego dodatniego fragmentu: " + zad.dlugoscMaksymalnegoCiaguDodatnich(tab));
        zad.odwrocFragment(tab,1,2);
        zad.signum(tab);
    }
}
