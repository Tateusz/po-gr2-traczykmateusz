package pl.edu.uwm.wmii.traczykmateusz.laboratorium02;
import java.util.*;
import java.util.stream.IntStream;


public class Zadania
{
    public static void generuj(int tab[])
    {
        Random r = new Random();
        for(int i=0; i<tab.length; i++)
        {
            tab[i] = r.nextInt(1999) -1000;
        }
        for(int i=0; i<tab.length; i++)
        {
            System.out.print(tab[i] + ", ");
        }
        System.out.print("\n ");
    }

    public static int ileNieparzystych(int tab[])
    {
        int counter = 0;
        for(int i=0; i<tab.length; i++)
        {
            boolean nieparzyste = IntStream.of(tab[i]).anyMatch(x -> x % 2 != 0);
            if (nieparzyste == true)   counter++;
        }
        return counter;
    }

    public static int ileParzystych(int tab[])
    {
        int counter = 0;
        for(int i=0; i<tab.length; i++)
        {
            boolean parzyste = IntStream.of(tab[i]).anyMatch(x -> x % 2 == 0);
            if (parzyste == true)   counter++;
        }
        return counter;
    }

    public static int ileDodatnich(int tab[])
    {
        int counter = 0;
        for(int i=0; i<tab.length; i++)
        {
            boolean dodatnie = IntStream.of(tab[i]).anyMatch(x -> x > 0);
            if (dodatnie == true)   counter++;
        }
        return counter;
    }

    public static int ileUjemnych(int tab[])
    {
        int counter = 0;
        for(int i=0; i<tab.length; i++)
        {
            boolean ujemne = IntStream.of(tab[i]).anyMatch(x -> x < 0);
            if (ujemne == true)   counter++;
        }
        return counter;
    }

    public static int ileZerowych(int tab[])
    {
        int counter = 0;
        for(int i=0; i<tab.length; i++)
        {
            boolean zerowe = IntStream.of(tab[i]).anyMatch(x -> x == 0);
            if (zerowe == true)   counter++;
        }
        return counter;
    }

    public static int ileMaksymalnych(int tab[])
    {
        int counter = 0;
        int max = Arrays.stream(tab).max().getAsInt();
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]==max) counter++;
        }
        return counter;
    }

    public static int sumaDodatnich(int tab[])
    {
        int suma = 0;
        for(int i=0; i<tab.length; i++)
        {
            boolean dodatnie = IntStream.of(tab[i]).anyMatch(x -> x > 0);
            if (dodatnie == true)   suma+=tab[i];
        }
        return suma;
    }

    public static int sumaUjemnych(int tab[])
    {
        int suma = 0;
        for(int i=0; i<tab.length; i++)
        {
            boolean ujemne = IntStream.of(tab[i]).anyMatch(x -> x < 0);
            if (ujemne == true)   suma+=tab[i];
        }
        return suma;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[])
    {
        int najdluzszy=0, temp=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]>0) najdluzszy++;
            else if(tab[i]<=0 && najdluzszy>temp)
            {
                temp=najdluzszy;
                najdluzszy=0;
            }
            else najdluzszy=0;
        }
        if(najdluzszy>temp) return najdluzszy;
        else return temp;
    }

    public static void signum(int tab[])
    {
        for(int i = 0; i<tab.length; i++)
        {
            tab[i] = Integer.signum(tab[i]);
        }
        System.out.println("Tablica po funkcji signum: ");
        for(int i=0; i<tab.length; i++)
        {
            System.out.println(tab[i] + " ");
        }
    }

    public static void odwrocFragment(int tab[], int lewy, int prawy)
    {
        for(int i=lewy; i<prawy; i++)
        {
            int temp = tab[i];
            tab[i] = tab[prawy];
            tab[prawy] = temp;
            prawy--;
        }
        for(int i=0; i<tab.length; i++)
        {
            System.out.print(tab[i] + ", ");
        }
        System.out.print("\n ");
    }

}
