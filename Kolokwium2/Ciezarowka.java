package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;
import java.util.*;

public class Ciezarowka {
    private double pojemnośćSilnika;
    private String nrVIN;
    public int rocznik;
    public String nazwa;
    public float srednieSpalanie;

    private enum typTrasy {
        A(0.9), B(1), C(1.2);

        private double numVal;

        typTrasy(double numVal) {
            this.numVal = numVal;
        }

        public double getNumVal() {
            return numVal;
        }
    }


    private Ciezarowka(double pojemnośćSilnika, String nrVIN, int rocznik, String nazwa, float srednieSpalanie) {
        this.pojemnośćSilnika = pojemnośćSilnika;
        this.nrVIN = nrVIN;
        this.rocznik = rocznik;
        this.nazwa = nazwa;
        this.srednieSpalanie = srednieSpalanie;
    }

    public void Create()
    {
        ArrayList<Ciezarowka> Ciezarowki = new ArrayList<Ciezarowka>();
        Ciezarowki.add(new Ciezarowka(9.6, "657", 1995, "Ciez1", 9.65f));
        Ciezarowki.add(new Ciezarowka(6.7, "846", 2005, "Ciez2", 6.56f));
        Ciezarowki.add(new Ciezarowka(10.1, "4563", 2010, "Ciez3", 10.12f));
    }

    public double ObliczSpalanie(double dlugoscTrasy)
    {
        return srednieSpalanie*dlugoscTrasy;
    }
}
