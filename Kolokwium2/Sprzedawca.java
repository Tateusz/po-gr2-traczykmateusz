package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;

public class Sprzedawca extends Pracownik{
    public String toString()
    {
        return "Sprzedawca ";
    }

    public String pracuj()
    {
        return "Pracownik teraz sprzedaje ";
    }
}
