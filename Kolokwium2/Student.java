package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Student implements IOsoba{
    private String imie;
    private String nazwisko;

    public Student(String imie, String nazwisko){
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public String getNazwisko(){
        return nazwisko;
    }

    public static void sort(ArrayList<Student> studenci){
        Collections.sort(studenci, Comparator.comparing(Student::getNazwisko));
        studenci.forEach(System.out::println);
    }

    @Override
    public String toString() {
        return imie + " " + nazwisko;
    }
}
