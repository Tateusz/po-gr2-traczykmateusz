package pl.edu.uwm.wmii.traczykmateusz.laboratorium12;
import java.util.*;

public class Main {

    public static void main(String[] args)
    {
        ArrayList<Pracownik> pracownicy = new ArrayList<Pracownik>();
        pracownicy.add(new Muzyk());
        pracownicy.add(new Ksiegowa());
        pracownicy.add(new Sprzedawca());

        for(Pracownik i: pracownicy)
        {
            System.out.println(i.pracuj());
        }

        ArrayList<Student> studenci = new ArrayList<Student>();
        studenci.add(new Student("Mariusz", "Kowalski"));
        studenci.add(new Student("Adam", "Michalik"));
        studenci.add(new Student("Tomasz", "Drabik"));
        studenci.add(new Student("Mateusz", "Traczyk"));
        studenci.add(new Student("Marek", "Nowak"));
        studenci.add(new Student("Mariusz", "Zawadzki"));

        System.out.println();
        System.out.println("Lista studentów: ");
        Student.sort(studenci);

        System.out.println(studenci);

    }
}

interface IOsoba{
    default String Opisz(String imie, String nazwisko) {
        return imie + " " + nazwisko;
    }
}
