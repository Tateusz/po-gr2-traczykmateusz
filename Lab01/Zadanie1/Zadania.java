package pl.edu.uwm.wmii.traczykmateusz.laboratorium01;

public class Zadania {
    public int wynik, wynik2;
    public double wynik1;

    public int zad1A(int tab[])
    {
        wynik=0;
        for(int i=0; i<tab.length; i++)
        {
            wynik += tab[i];
        }
        return wynik;
    }

    public int zad1B(int tab[])
    {
        wynik=1;
        for(int i=0; i<tab.length; i++)
        {
            wynik *= tab[i];
        }
        return wynik;
    }

    public int zad1C(int tab[])
    {
        wynik=0;
        for(int i=0; i<tab.length;i++)
        {
            wynik +=  Math.abs(tab[i]);
        }
        return wynik;
    }

    public double zad1D(int tab[])
    {
        wynik=0;
        for(int i=0; i<tab.length; i++)
        {
            wynik += Math.sqrt(Math.abs(tab[i]));
        }
        return wynik;
    }

    public int zad1E(int tab[])
    {
        wynik = 1;
        for(int i=0; i<tab.length; i++)
        {
            wynik *= Math.abs(tab[i]);
        }
        return wynik;
    }

    public int zad1F(int tab[])
    {
        wynik = 0;
        for(int i=0; i<tab.length; i++)
        {
            wynik += Math.pow(tab[i], 2);
        }
        return wynik;
    }

    public void zad1G(int tab[])
    {
        wynik1=0;
        wynik2=1;
        for(int i=0; i<tab.length; i++)
        {
            wynik1 +=tab[i];
            wynik2 *=tab[i];
        }
        System.out.println("g): " + wynik1 + "  " + wynik2);
    }

    public int zad1H(int tab[])
    {
        wynik=0;
        for(int i=0; i<tab.length; i++)
        {
            wynik+= Math.pow(-1, i+1) * tab[i];
        }
        return wynik;
    }

    public double zad1I(int tab[])
    {
        wynik2=0;
        wynik1=1;
        for(int i=0; i<tab.length; i++)
        {
            wynik1*=i+1;
            wynik2+= ((tab[i] * Math.pow(-1, i))/wynik1);
        }
        return wynik2;
    }
}
