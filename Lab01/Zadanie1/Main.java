package pl.edu.uwm.wmii.traczykmateusz.laboratorium01;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Zadania zad = new Zadania();
        int rozmiar, liczby;
        System.out.println("Zadanie 1_1");
        do {
            Scanner liczba = new Scanner(System.in);
            System.out.print("Podaj ilosc liczb: "); //ilosc liczb
            rozmiar = liczba.nextInt();
        } while (rozmiar < 0);
        int tab[] = new int[rozmiar];

        for (int i = 0; i < rozmiar; i++) {
            Scanner liczbaNaturalna = new Scanner(System.in);
            System.out.print("Podaj " + (i + 1) + " liczbe naturalna:");  //wczytanie liczb
            liczby = liczbaNaturalna.nextInt();
            tab[i] = liczby;
        }

        System.out.println("a): " + zad.zad1A(tab));
        System.out.println("b): " + zad.zad1B(tab));
        System.out.println("c): " + zad.zad1C(tab));
        System.out.println("d): " + zad.zad1D(tab));
        System.out.println("e): " + zad.zad1E(tab));
        System.out.println("f): " + zad.zad1F(tab));
        zad.zad1G(tab);
        System.out.println("h): " + zad.zad1H(tab));
        System.out.println("i): " + zad.zad1I(tab));

//Zadanie 1_2

        System.out.println("Zadanie 1_2");
        int liczbaN, licz1;
        do {
            Scanner liczba = new Scanner(System.in);
            System.out.print("Podaj ilosc liczb: ");
            liczbaN = liczba.nextInt();
        } while (liczbaN < 0);

        int tab2[] = new int[liczbaN];

        for (int i = 0; i < liczbaN; i++) {
            do {
                Scanner liczba2 = new Scanner(System.in);
                System.out.print("Podaj liczbe rzeczywista: ");
                licz1 = liczba2.nextInt();
            } while (licz1 < 0);
            tab2[i] = licz1;
        }

        for (int i = 0; i < liczbaN; i++) {
            if (i == liczbaN - 1) System.out.print(tab2[0]);
            else System.out.print(tab2[i + 1] + " ");
        }
    }
}
