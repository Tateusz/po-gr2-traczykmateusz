package pl.edu.uwm.wmii.traczykmateusz.laboratorium01;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Zadania zad = new Zadania();

        int rozmiar, liczby;
        do {
            Scanner liczba = new Scanner(System.in);
            System.out.print("Podaj ilosc liczb: ");
            rozmiar = liczba.nextInt();
        } while (rozmiar < 0);
        int tab[] = new int[rozmiar];

        for (int i = 0; i < rozmiar; i++) {
            do {
                Scanner liczbaNaturalna = new Scanner(System.in);
                System.out.print("Podaj liczbe naturalna: ");
                liczby = liczbaNaturalna.nextInt();
            } while (liczby < 0);
            tab[i] = liczby;
        }
        System.out.println("a): " + zad.zad2A(tab));
        System.out.println("b): " + zad.zad2B(tab));
        System.out.println("c): " + zad.zad2C(tab));
        System.out.println("d): " + zad.zad2D(tab));
        System.out.println("e): " + zad.zad2E(tab));
        System.out.println("f): " + zad.zad2F(tab));
        System.out.println("g): " + zad.zad2G(tab));
        System.out.println("h): " + zad.zad2H(tab));
    }
}
