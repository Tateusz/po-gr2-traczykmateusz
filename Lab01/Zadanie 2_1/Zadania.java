package pl.edu.uwm.wmii.traczykmateusz.laboratorium01;

public class Zadania
{
    public int ilosc=0, silnia=1;
    public String liczba;
    public char znak;

    public int zad2A(int tab[])
    {
        ilosc=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]%2==1) ilosc++;
        }
        return ilosc;
    }

    public int zad2B(int tab[])
    {
        ilosc=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]%3 == 0 && tab[i]%5 != 0)
            {
                ilosc++;
            }
        }
        return ilosc;
    }

    public int zad2C(int tab[])
    {
        ilosc=0;
        for(int i=0; i<tab.length; i++)
        {
            if(Math.sqrt(tab[i])%2 == 0) ilosc++;
        }
        return ilosc;
    }

    public int zad2D(int tab[])
    {
        ilosc=0;
        for(int i=1; i<tab.length-1; i++)
        {
            if((tab[i-1] + tab[i+1])/2 > tab[i])
                ilosc++;
        }
        return ilosc;
    }

    public int zad2E(int tab[])
    {
        ilosc=0;
        for(int i=0; i<tab.length; i++)
        {
            if(Math.pow(2, i+1) < tab[i] && tab[i] < silnia)
            {
                ilosc++;
            }
            silnia *=(i+1);
        }
        silnia=1;
        return ilosc;
    }

    public int zad2F(int tab[])
    {
        ilosc=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]%2==0)
            {
                liczba = Integer.toString(tab[i]);
                for(int j=0; j<=liczba.length() - 1; j++)
                {
                    znak = liczba.charAt(j);
                    if(znak == '1' || znak == '3' || znak == '5' || znak == '7' || znak == '9')
                    {
                        ilosc++;
                        break;
                    }
                }
            }
        }
        return ilosc;
    }

    public int zad2G(int tab[])
    {
        ilosc=0;
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]%2==1 && tab[i]>=0) ilosc++;
        }
        return ilosc;
    }

    public int zad2H(int tab[])
    {
        ilosc=0;
        for(int i=0; i<tab.length; i++)
        {
            if(Math.abs(tab[i]) < Math.pow(i, 2)) ilosc++;
        }
        return ilosc;
    }
}
