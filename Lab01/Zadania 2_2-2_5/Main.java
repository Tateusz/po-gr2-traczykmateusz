package pl.edu.uwm.wmii.traczykmateusz.laboratorium01;
import java.util.*;

public class Main {


    public static void main(String[] args)
    {
        int n;
//wczytanie liczb
        do{
            Scanner inN = new Scanner(System.in);
            System.out.print("Podaj ilosc liczb: ");
            n = inN.nextInt();
        }while(n<1);

        double tab[] = new double[n];

        for (int i = 0; i < n; i++)
        {
            Scanner inLiczba = new Scanner(System.in);
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = inLiczba.nextDouble();
        }
//zadanie 2_2
        double suma=0;
        for (int i = 0; i < n; i++) {
            if(tab[i]>0) suma+=tab[i];
        }
        suma*=2;
        System.out.println("Zadanie 2_2: ");
        System.out.println("Podwojona suma liczb: " + suma);

//zadanie 2_3
        int dodatnie = 0, ujemne = 0, zera = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] > 0) dodatnie++;
            else if (tab[i] == 0) zera++;
            else ujemne++;
        }
        System.out.println("Zadanie 2_3: ");
        System.out.println("Dodatnie: " +dodatnie);
        System.out.println("Ujemne: " +ujemne);
        System.out.println("Zera: " +zera);

//zadanie 2_4
        double min=tab[0], maks=tab[0];
        for (int i = 1; i < n ; i++) {
            if(tab[i]<min) min=tab[i];
            if(tab[i]>maks) maks=tab[i];
        }
        System.out.println("Zadanie 2_4: ");
        System.out.println("Minimum: " + min);
        System.out.println("Maksimum: " + maks);

//zadanie 2_5
        int licznik = 0;
        for(int i=0; i<tab.length-1; i++)
        {
            if(tab[i] > 0 && tab[i+1] > 0)
                licznik++;
        }
        System.out.println("Zadanie 2_5: ");
        System.out.println("Ilosc par spełniających warunek: " + licznik);
    }
}
