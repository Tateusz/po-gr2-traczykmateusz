package pl.edu.uwm.wmii.traczykmateusz.laboratorium04;
import java.math.BigInteger;
import java.math.BigDecimal;

public class Main {

    public static void main(String[] args)
    {
        System.out.println("Ilosc ziaren na szachownicy: " + ziarna(3));
        System.out.println("Kapital po okresie lokaty: " + kapital(1500,6,2));
    }

    public static BigInteger ziarna(int n)
    {
        BigInteger bigInt = BigInteger.valueOf(n*n);
        BigInteger wynik = BigInteger.valueOf(2);
        wynik = wynik.pow(n*n).subtract(BigInteger.valueOf(1));
        return wynik;
    }

    public static BigDecimal kapital(int k, double p, int n)
    {
        BigDecimal wynik = BigDecimal.valueOf(k).multiply(BigDecimal.valueOf(n*30).multiply(BigDecimal.valueOf(p/100)));
        wynik = wynik.divide(BigDecimal.valueOf(365),BigDecimal.ROUND_UP).add(BigDecimal.valueOf(k));
        return wynik;
    }

}
