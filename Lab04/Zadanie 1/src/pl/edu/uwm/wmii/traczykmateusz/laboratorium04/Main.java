package pl.edu.uwm.wmii.traczykmateusz.laboratorium04;
import java.lang.Math;

public class Main {

    public static void main(String[] args) {
        System.out.println("a): Wystapienia szukanej litery: " + countChar("ambalabamba", 'b'));
        System.out.println("b): Wystapienia szukanego substringa: " + countSubStr("lalela", "la"));
        System.out.println("c): Srodek stringa: " + middle("middle"));
        System.out.println("d):" + repeat("ho",5));
        System.out.println("f):" + change("aBcD"));
        System.out.println("g):" + nice("001001010100110111011"));
        System.out.println("h):" + niceUpgrade("001001010100110111011", ";", 4));
    }

    public static int countChar(String str, char c)
    {
        int counter = 0;
        for (int i = 0; i < str.length(); i++)
        {
            if (str.charAt(i) == c) counter++;
        }
        return counter;
    }

    public static int countSubStr(String str, String subStr)
    {
        int lastIndex = 0;
        int counter = 0;

        while (lastIndex != -1)
        {
            lastIndex = str.indexOf(subStr, lastIndex);
            if (lastIndex != -1)
            {
                counter++;
                lastIndex += subStr.length();
            }
        }
        return counter;
    }

    public static String middle(String str)
    {
        int mid = 0;
        if(str.length()%2==0)
        {
            mid = str.length()/2-1;
            return str.substring(mid, mid + 2);
        }
        else
        {
            return str.substring(str.length()/2,str.length()/2+1);
        }
    }

    public static String repeat(String str, int n)
    {
        String concat = "";
        for(int i=0; i<n; i++)
        {
            concat+=str;
        }
        return concat;
    }

    public static String change(String str)
    {
        StringBuffer sBuffer=new StringBuffer(str);
        for(int i = 0; i < str.length(); i++)
        {
            if(Character.isLowerCase(str.charAt(i)))
            {
                sBuffer.setCharAt(i, Character.toUpperCase(str.charAt(i)));
            }
            else if(Character.isUpperCase(str.charAt(i)))
            {
                sBuffer.setCharAt(i, Character.toLowerCase(str.charAt(i)));
            }
        }
        return sBuffer.toString();
    }

    public static String nice(String str)
    {
        StringBuffer sBuffer=new StringBuffer(str);
        for(int i=str.length()-3; i>=0; i-=3)
        {
            sBuffer.insert(i,"'");
        }
        return sBuffer.toString();
    }

    public static String niceUpgrade(String str, String sep, int coIle)
    {
        StringBuffer sBuffer=new StringBuffer(str);
        for(int i=str.length()-coIle; i>=0; i-=coIle)
        {
            sBuffer.insert(i, sep);
        }
        return sBuffer.toString();
    }
    
}
