package pl.edu.uwm.wmii.traczykmateusz.laboratorium04;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        System.out.println("Wystapienia szukanego znaku: " + szukajZnak('a', "dane.txt"));
        System.out.println("Wystapienia szukanego ciagu znakow: " + szukajCiag("ba", "dane.txt"));
    }

    public static int szukajZnak(char str, String nazwaPliku) throws IOException
    {
        int counter = 0;
        Path path = Path.of(nazwaPliku);
        String czytaj = Files.readString(path);

        for (int i = 0; i < czytaj.length(); i++)
        {
            if (czytaj.charAt(i) == str) counter++;
        }
        return counter;
    }

    public static int szukajCiag(String str, String nazwaPliku) throws IOException
    {
        int counter = 0;
        int lastIndex = 0;
        Path path = Path.of(nazwaPliku);
        String czytaj = Files.readString(path);

        while (lastIndex != -1)
        {
            lastIndex = czytaj.indexOf(str, lastIndex);
            if (lastIndex != -1)
            {
                counter++;
                lastIndex += str.length();
            }
        }
        return counter;
    }
}
